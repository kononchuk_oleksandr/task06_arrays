package com.axeane.generics.units;

public abstract class Unit implements Comparable {
  private String name;

  public Unit(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public int compareTo(Object o) {
    return 0;
  }
}
