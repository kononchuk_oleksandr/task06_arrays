/**1. Generic class – container with units.
 * You can put and get units from container.
 */

package com.axeane.generics;

import com.axeane.generics.units.Laptop;
import com.axeane.generics.units.Unit;
import java.util.List;

public class ContainerExample<T extends Unit> {
  private T value;

  public T getValue() {
    return value;
  }

  public void setValue(T value) {
    this.value = value;
  }

  /** Method with using wildcard
   */
  public boolean find(List<? extends Unit> list, Unit unit) {
    for (Unit value : list) {
      if (unit.equals(value)) {
        return true;
      }
    }
    return false;
  }
  
  public void addToList(List<? super Laptop> list, Laptop laptop) {
    list.add(laptop);
  }
}
