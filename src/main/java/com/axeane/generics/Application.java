package com.axeane.generics;

import com.axeane.generics.units.Laptop;
import com.axeane.generics.units.Phone;
import com.axeane.generics.units.Tablet;
import com.axeane.generics.units.Unit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
  private static Logger logger = LogManager.getLogger(Application.class);

  public static void main(String[] args) {

    ContainerExample<Unit> container = new ContainerExample<>();
    container.setValue(new Phone("iPhone"));

    //Testing methods with using wildcard
    Tablet tablet = new Tablet("iPad");
    List<Tablet> tabletList = new ArrayList<>(Arrays.asList(tablet));
    boolean result = container.find(tabletList, tablet);
    logger.info(result);

    List<Unit> laptopList = new ArrayList<>();
    container.addToList(laptopList, new Laptop("Lenovo"));
    for(Unit unit : laptopList) {
      logger.info(unit.getName());
    }

    //Testing PriorityQueue
    PriorityQueue<Unit> queue = new PriorityQueueExample<>();
    queue.add(new Phone("iPhone"));
    queue.add(new Tablet("iPad"));
    queue.add(new Laptop("Lenovo"));

    logger.info("The queue is:");
    for(Unit unit : queue) {
      logger.info(unit.getName());
    }

    //3. Trying to add string into List<Integer>.
    List<Integer> integers = new LinkedList<>();
    try {
      integers.add((Integer.parseInt("Hello")));
    } catch (NumberFormatException e) {
      logger.error("NumberFormatException " + e.getMessage());
    }

  }
}
