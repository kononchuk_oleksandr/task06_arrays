package com.axeane.arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.*;

public class ArraysLogicalTasks {
  private static Logger logger = LogManager.getLogger(ArraysLogicalTasks.class);
  private static List<Integer> listOne = new ArrayList<Integer>(
      java.util.Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9 ,10));
  private static List<Integer> listTwo = new ArrayList<Integer>(
      java.util.Arrays.asList(6, 7, 8, 9, 10, 11, 12, 13, 14, 15));
  private static List<Integer> listThree = new ArrayList<Integer>(
      java.util.Arrays.asList(1, 1, 2, 2, 3, 3, 4, 1, 1, 10, 10, 10, 11, 12, 13, 13));


  public static void main(String[] args) {
    taskA(listOne, listTwo);
    //taskB(listThree); comment taskC if you want test taskB
    taskC(listThree);
  }

  /**
   * A. Формує третій масив, що складається з елементів, які:
   *  а) присутні в обох масивах;
   *  б) присутні тільки в одному з масивів.
   *  */
  private static void taskA(List<Integer> list1, List<Integer> list2) {
    List<Integer> twoArraysSummaryList = new ArrayList<>();
    List<Integer> uniqueElementsList = new ArrayList<>();

    // a)
    for (Integer value1 : list1) {
      for (Integer value2 : list2) {
        if (value2.equals(value1)) {
          twoArraysSummaryList.add(value2);
        }
      }
    }

    // b)
    for (Integer number : list1) {
      if (!list2.contains(number)) {
        uniqueElementsList.add(number);
      }
    }

    logger.info("Two arrays contains this numbers: " + String.join(", ",
        twoArraysSummaryList.toString()));
    logger.info("Array nr 1 has unique numbers: " + String.join(", ",
        uniqueElementsList.toString()));
  }

  /**
   * B. Видаляє в масиві всі числа, які повторюються більше двох разів.
   */
  private static void taskB(List<Integer> list) {
    int number, count;
    for (int i = 0; i < list.size(); i++) {
        number = list.get(i);
        count = 1;
        for (int j = 1; j < list.size(); j++) {
          if (number == list.get(j)) {
            count++;
            if (count > 2) {
              list.removeAll(Collections.singleton(number));
            }
          }
        }
    }
    logger.info("List: " + String.join(", ",
        list.toString()));
  }

  /**
   * Знаходить в масиві всі серії однакових елементів,
   * які йдуть підряд, і видаляє з них всі елементи крім одного.
   */
  private static void taskC(List<Integer> list) {

    for (int i = 0; i < list.size() - 1; ) {
      if (list.get(i).equals(list.get(i + 1))) {
        list.remove(i + 1);
      } else {
        i++;
      }
    }

    logger.info("List: " + String.join(", ",
        list.toString()));
  }
}
