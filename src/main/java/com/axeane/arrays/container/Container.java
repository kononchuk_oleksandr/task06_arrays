/**
 * 2.
 * Create a container that encapsulates an array of String,
 * and that only adds Strings and gets Strings, so that there are no casting
 * issues during use. If the internal array isn’t big enough for the next add,
 * your container should automatically resize it.
 */

package com.axeane.arrays.container;

import java.util.Arrays;

public class Container<T extends String> {
  private String[] array = new String[1];

  public Container() {
  }

  public Container(T... args) {
    addAll(args);
  }

  public String get(int index) {
    return this.array[index];
  }

  public String[] getAll() {
    return this.array;
  }

  public void add(int position, T element) {
    if (position > 0) {
      this.array = Arrays.copyOf(this.array, position + 1);
    }
    this.array[position] = element;
  }

  public void addAll(T... args) {
    this.array = new String[args.length];
    for (int i = 0; i < args.length; i++) {
      this.array[i] = args[i];
    }
  }
}
