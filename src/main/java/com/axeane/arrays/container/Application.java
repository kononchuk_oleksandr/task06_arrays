/**
 * 2.
 * main() compares the performance of container
 * with an ArrayList holding Strings.
 */

package com.axeane.arrays.container;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
  private static Logger logger = LogManager.getLogger(Application.class);
  private static Container<String> container = new Container<>();
  private static List<String> list = new ArrayList<>();

  public static void main(String[] args) {
    //ArrayList add time
    long startTime = System.nanoTime();
    for (int i = 0; i < 100; i++) {
      list.add("Hello World!");
    }
    long endTime = System.nanoTime();
    long duration = endTime - startTime;
    logger.info("ArrayList ADD time is: " + duration);

    //Container add time
    startTime = System.nanoTime();
    for (int i = 0; i < 100; i++) {
      container.add(i, "Hello World!");
    }
    endTime = System.nanoTime();
    duration = endTime - startTime;
    logger.info("Container ADD time is: " + duration);

    //ArrayList get time
    startTime = System.nanoTime();
    for (int i = 0; i < 100; i++) {
      list.get(i);
    }
    endTime = System.nanoTime();
    duration = endTime - startTime;
    logger.info("ArrayList GET time is: " + duration);

    //Container get time
    startTime = System.nanoTime();
    for (int i = 0; i < 100; i++) {
      container.get(i);
    }
    endTime = System.nanoTime();
    duration = endTime - startTime;
    logger.info("Container GET time is: " + duration);
  }
}
