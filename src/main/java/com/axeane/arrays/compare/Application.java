/**
 * UNCOMPLETED! Need to do logic for Array end perform a binary search.
 *
 * TASK 3.
 * Create a class containing two String objects and make it
 * Comparable so that the comparison only cares about the first String.
 * Fill an array and an ArrayList with objects of your class by using
 * a custom generator (eg, which generates pairs of Country-Capital).
 * Demonstrate that sorting works properly. Now make a Comparator that
 * only cares about the second String and demonstrate that sorting works properly.
 * Also perform a binary search using your Comparator.
 */

package com.axeane.arrays.compare;

import com.axeane.arrays.compare.Countries.CountryComparator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
  private static Logger logger = LogManager.getLogger(Application.class);

  public static void main(String[] args) {
    List<Countries> list = new CountryGenerator().countryGeneratorList();

    logger.info("Unsorted list:");
    for (Countries c: list) {
      logger.debug(c.toString());
    }

    logger.info("Sorted list by capital:");
    list.sort(new Countries.CapitalComparator());
    for (Countries c : list) {
      logger.debug(c.toString());
    }

    logger.info("Sorted list by country:");
    list.sort(new CountryComparator());
    for (Countries c : list) {
      logger.debug(c.toString());
    }
  }

}
