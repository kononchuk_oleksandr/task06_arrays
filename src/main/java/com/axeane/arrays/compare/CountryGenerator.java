package com.axeane.arrays.compare;

import java.util.ArrayList;
import java.util.List;

public class CountryGenerator {

  public List<Countries> countryGeneratorList() {
    List<Countries> list = new ArrayList<>();
    list.add(new Countries("Ukraine", "Kyiv"));
    list.add(new Countries("Poland", "Warsaw"));
    list.add(new Countries("Spain", "Madrid"));
    list.add(new Countries("Germany", "Berlin"));
    list.add(new Countries("USA", "Washington"));
    return list;
  }

}
