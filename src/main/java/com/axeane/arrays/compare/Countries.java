package com.axeane.arrays.compare;

import java.util.Comparator;

public class Countries {

  private String country;
  private String capital;

  Countries(String country, String capital) {
    this.country = country;
    this.capital = capital;
  }

  public String getCountry() {
    return country;
  }

  public String getCapital() {
    return capital;
  }

  @Override
  public String toString() {
    return "Countries{" +
        "country='" + country + '\'' +
        ", capital='" + capital + '\'' +
        '}';
  }

  public static class CapitalComparator implements Comparator<Countries> {

    @Override
    public int compare(Countries c1, Countries c2) {
      return c2.capital.compareTo(c1.capital);
    }
  }

  public static class CountryComparator implements Comparator<Countries> {

    @Override
    public int compare(Countries c1, Countries c2) {
      return c2.country.compareTo(c1.country);
    }
  }
}
