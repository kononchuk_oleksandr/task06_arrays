package com.axeane.arrays.game;

public class Monster extends Objects{

  public Monster(int strength) {
    super(strength);
  }

  @Override
  public int getStrength() {
    return super.getStrength();
  }

  @Override
  public String toString() {
    return "monster with strength " + getStrength();
  }
}
