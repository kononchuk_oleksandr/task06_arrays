/**
 * D. Герой комп'ютерної гри, що володіє силою в 25 балів, знаходиться в круглому залі,
 * з якого ведуть 10 закритих дверей. За кожними дверима героя чекає або магічний артефакт,
 * що дарує силу від 10 до 80 балів, або монстр, який має силу від 5 до 100 балів,
 * з яким герою потрібно битися. Битву виграє персонаж, що володіє найбільшою силою; якщо сили рівні, перемагає герой.
 * 1. Організувати введення інформації про те, що знаходиться за дверима, або заповнити її,
 * використовуючи генератор випадкових чисел.
 * 2. Вивести цю саму інформацію на екран в зрозумілому табличному вигляді.
 * 3. Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.
 * 4. Вивести номери дверей в тому порядку, в якому слід їх відкривати герою, щоб залишитися в живих, якщо таке можливо.
 */

package com.axeane.arrays.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Logic {

  private static Logger logger = LogManager.getLogger(Logic.class);
  private Hero hero = new Hero();

  /**
   * 1. Заповнює інформацією, що знаходиться за дверима
   *    використовуючи генератор випадкових чисел.
   *    Артефакти: від 10 до 80.
   *    Монстри: від 5 до 100.
   */
  private List<Objects> doorList() {
    List<Objects> doors = new ArrayList<>();
    doors.add(new Artifact(new Random().nextInt(70 + 1) + 10));
    doors.add(new Monster(new Random().nextInt(95 + 1) + 5));
    doors.add(new Artifact(new Random().nextInt(70 + 1) + 10));
    doors.add(new Monster(new Random().nextInt(95 + 1) + 5));
    doors.add(new Artifact(new Random().nextInt(70 + 1) + 10));
    doors.add(new Monster(new Random().nextInt(95 + 1) + 5));
    doors.add(new Artifact(new Random().nextInt(70 + 1) + 10));
    doors.add(new Monster(new Random().nextInt(95 + 1) + 5));
    doors.add(new Artifact(new Random().nextInt(70 + 1) + 10));
    doors.add(new Monster(new Random().nextInt(95 + 1) + 5));
    return doors;
  }

  /**
   * 2. Виводить інформацію про те, що знаходиться за дверима
   *    в зрозумілому табличному вигляді.
   */
  public void getDoors() {
    for (int i = 0; i < doorList().size(); i++) {
      logger.info("Behind the door nr. " + i + " is " + doorList().get(i).toString());
    }
  }

  /**
   * 3. Рахує, за скількома дверима героя чекає смерть БЕЗ РЕКУРСІЇ.
   */
  public void countDoorsToDeath() {
    int count = 0;
    int power = hero.getStrength();
    for (int i = 0; i < doorList().size(); i++) {
      if (power < -1) {
        logger.info("Hero is dead after opening " + count + " doors");
        break;
      } else {
        if (doorList().get(i) instanceof Artifact) {
          power += doorList().get(i).getStrength();
          count++;
        } else if (doorList().get(i) instanceof Monster) {
          power -= doorList().get(i).getStrength();
          count++;
        }
      }
    }
    if (power >= 0) {
      logger.info("Hero is alive with " + power + " strength!");
    }
  }
}
