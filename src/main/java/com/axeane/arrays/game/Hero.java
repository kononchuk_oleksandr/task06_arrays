package com.axeane.arrays.game;

public class Hero {
  private int strength;

  public Hero() {
    strength = 25;
  }

  public int getStrength() {
    return strength;
  }

  public void setStrength(int strength) {
    this.strength = strength;
  }
}
