package com.axeane.arrays.game;

public class Artifact extends Objects {

  public Artifact(int strength) {
    super(strength);
  }

  @Override
  public int getStrength() {
    return super.getStrength();
  }

  @Override
  public String toString() {
    return "artifact with strength " + getStrength();
  }
}
