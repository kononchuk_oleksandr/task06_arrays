package com.axeane.arrays.game;

public abstract class Objects {
  private int strength;

  public Objects(int strength) {
    this.strength = strength;
  }

  public int getStrength() {
    return strength;
  }
}
